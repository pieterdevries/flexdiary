/**
 * (c) Frank Meyer, 2014
 * All Rights Reserved
 * No part of this software or any of its contents may be reproduced, copied, 
 * modified or adapted, without the prior written consent of the author, 
 * unless otherwise indicated for stand-alone materials.
 */

package de.meyer.FlexDiary.services;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;

import de.meyer.FlexDiary.data.Diary;
import de.meyer.FlexDiary.data.Entry;

/**
 * 
 * @author Frank Meyer
 */
public class EntryServices {

	static Logger log = Logger.getLogger(EntryServices.class);

	@PersistenceContext
	private EntityManager em;
	private EntityManagerFactory emf;
	
	
	public EntryServices() {

	}

	private void init() {
		this.emf = Persistence.createEntityManagerFactory("FlexDiaryPU");
		this.em = emf.createEntityManager();
		this.em.getTransaction().begin();

	}

	private void finish() {
		this.em.close();
	}

	public void saveNewEntry(Entry arg) {

		this.init();
		this.em.persist(arg);
		this.finish();
	}

	public void updateEntry(Entry arg) {

		this.init();
		this.em.merge(arg);
		this.finish();

	}

	public List getEntries(Diary diary, int numberOfEntries, int upSet,
			Date fromDate, Date toDate) {
		
		this.init();
		
		

		return null;
	}
}
