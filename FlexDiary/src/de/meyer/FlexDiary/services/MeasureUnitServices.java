package de.meyer.FlexDiary.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import de.meyer.FlexDiary.data.MeasureUnit;
import de.meyer.FlexDiary.data.TypeOfMeasureUnits;

public class MeasureUnitServices {

	static Logger log = Logger.getLogger(MeasureUnitServices.class); 

	@PersistenceContext
	private EntityManager em;
	private EntityManagerFactory emf;
	
	private List<MeasureUnit> bsmUList;
	private List<MeasureUnit> cmUList;
	private List<MeasureUnit> hmUList;
	private List<MeasureUnit> wmUList;
	private List<MeasureUnit> imUList;
	
	public MeasureUnitServices() {

	}

	private void init() {
		this.emf = Persistence.createEntityManagerFactory("FlexDiaryPU");
		this.em = emf.createEntityManager();
		this.em.getTransaction().begin();
	}

	private void finish() {

		this.em.close();

	}

	public void saveNewMeasueUnit(MeasureUnit arg) {

		this.init();
		this.em.persist(arg);
		this.em.getTransaction().commit();
		this.log.debug("New MU was created - " + arg.getLabel());
		this.finish();
	}

	public void updateMeasureUnit(MeasureUnit arg) {

		this.init();
		this.em.merge(arg);
		this.finish();
	}

	public MeasureUnit findMU(String id) {

		this.init();
		MeasureUnit tempmu = em.find(MeasureUnit.class, id);
		this.em.getTransaction().commit();
		return tempmu;

	}


	public List<MeasureUnit> getAll(TypeOfMeasureUnits arg) {

		this.init();

		Query q;

		q = em.createQuery("select n from MeasureUnit n where n.type='"
				+ arg.toString() + "'");

		if (q.getMaxResults() == 0) {
			this.log.warn("No MeasureUnit loaded from database");
			return null;
		}
		
		List<MeasureUnit> tmpData = q.getResultList();

		this.em.getTransaction().commit();

		return tmpData;
	}

	public List<MeasureUnit> getBsmUList() {

		this.bsmUList = this.getAll(TypeOfMeasureUnits.BLOODSUGAR);
		this.log.debug("Bloodsugar MU are loaded from database");
		return bsmUList;
	}

	public void setBsmUList(List<MeasureUnit> bsmUList) {
		this.bsmUList = bsmUList;
	}

	public List<MeasureUnit> getCmUList() {

		this.cmUList = this.getAll(TypeOfMeasureUnits.CARBOHYDRATE);
		this.log.debug("Carbohydrate MU are loaded from database");
		return cmUList;
	}

	public void setCmUList(List<MeasureUnit> cmUList) {
		this.cmUList = cmUList;
	}

	public List<MeasureUnit> getHmUList() {

		this.hmUList = this.getAll(TypeOfMeasureUnits.HEIGHT);
		this.log.debug("Height MU are loaded from database");
		return hmUList;
	}

	public void setHmUList(List<MeasureUnit> hmUList) {
		this.hmUList = hmUList;
	}

	public List<MeasureUnit> getWmUList() {

		this.wmUList = this.getAll(TypeOfMeasureUnits.WEIGHT);
		this.log.debug("Weight MU are loaded from database");
		return wmUList;
	}

	public void setWmUList(List<MeasureUnit> wmUList) {

		this.wmUList = wmUList;
	}

	public List<MeasureUnit> getImUList() {

		this.imUList = this.getAll(TypeOfMeasureUnits.INSULIN);
		this.log.debug("Insulin MU are loaded from database");
		return imUList;
	}

	public void setImUList(List<MeasureUnit> imUList) {
		this.imUList = imUList;
	}

}
