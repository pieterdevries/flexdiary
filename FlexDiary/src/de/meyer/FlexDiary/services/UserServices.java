/**
 * (c) Frank Meyer, 2014
 * All Rights Reserved
 * No part of this software or any of its contents may be reproduced, copied, 
 * modified or adapted, without the prior written consent of the author, 
 * unless otherwise indicated for stand-alone materials.
 */

package de.meyer.FlexDiary.services;

import de.meyer.FlexDiary.data.Diary;
import de.meyer.FlexDiary.data.UserData;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * 
 * @author Frank Meyer
 */
public class UserServices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static Logger log = Logger.getLogger(UserServices.class); 

	@PersistenceContext
	private EntityManager em;
	private EntityManagerFactory emf;

	public UserServices() {
		

	}

	private void init() {
		this.emf = Persistence.createEntityManagerFactory("FlexDiaryPU");
		this.em = emf.createEntityManager();
		this.em.getTransaction().begin();

	}

	private void finish() {
		this.em.close();
	}

	public UserData findUserById(String arg) {
		this.init();

		return null;
	}

	public UserData findUserById(UserData arg) {

		return null;
	}

	/**
	 * 
	 * @param arg
	 */
	public void saveUser(UserData arg) {

		this.init();

		if (this.getDefaultDiary(arg) == null) {
			this.em.persist(arg);
			this.em.getTransaction().commit();
			this.log.debug("New user persisted.");
		}else {
			Diary diary = new Diary();
			diary.setUserId(arg.getId());
			diary.setDefaultdiary(true);
			diary.setName("default");
			
			this.saveNewDiary(diary);
			
			this.init();
			this.em.persist(arg);
			this.em.getTransaction().commit();
			this.log.debug("New user and new diary persisted.");			
		}

		this.finish();

	}

	public void updateUser(UserData arg) {

		this.init();

		this.em.merge(arg);
		this.em.getTransaction().commit();
		this.log.debug("User updated :" + arg.getUsername());
		this.finish();
	}

	public void deleteUser(UserData arg) {

		this.init();

		this.em.remove(em.merge(arg));
		this.log.debug("User deleted:" + arg.getUsername());

		this.finish();

	}

	public boolean checkUsernameAlreadyInUse(String arg) {

		this.init();

		this.log.debug("Checking if username already in use.");
		Query q = em
				.createQuery("select count(k.username) from UserData k where k.username ='"
						+ arg + "' ");
		long counter = 0;
		counter = (Long) q.getSingleResult();

		this.finish();

		return (counter == 0);

	}

	public UserData isLoginCorrect(String username, String password) {

		this.init();

		Query query;
		query = em
				.createQuery("select n from UserData n where n.username= :name and n.password = :word");
		query.setParameter("name", username);
		query.setParameter("word", password);

		List<UserData> userList = query.getResultList();

		if (userList.size() == 1) {
			return userList.get(0);

		} else {
			return null;
		}
	}

	public Diary getDefaultDiary(UserData arg) {

		this.init();

		Query query;
		query = em
				.createQuery("select n from Diary n where n.userid = :user and n.defaultdiary = true");
		query.setParameter("user", arg.getId());

		List<Diary> diaryList = query.getResultList();

		this.log.debug("Number of result from searching for default diary "
				+ diaryList.size());

		if (diaryList.size() == 0) {
			this.log.debug("Search for default diary WITHOUT results for "
					+ arg.getUsername());
			return null;
		} else {
			this.log.debug("Search for default diary WITH results for "
					+ arg.getUsername());
			return diaryList.get(0);
		}
	}

	public void saveNewDiary(Diary arg) {

		this.init();

		this.em.persist(arg);
		this.em.getTransaction().commit();
		this.log.debug("New diary persisted.");

		this.finish();
	}

	public void updateDiary(Diary arg) {

		this.init();

		this.em.merge(arg);
		this.em.getTransaction().commit();
		this.log.debug("Diary updated :" + arg.getName());

		this.finish();

	}

	public Diary getCurrDiary(String name, UserData user) {

		this.init();

		Query query;
		query = em
				.createQuery("select n from Diary n where n.userid = :user and n.name = :diaryname");
		query.setParameter("user", user.getId());
		query.setParameter("diaryname", name);

		List<Diary> diaryList = query.getResultList();

		if (diaryList.size() == 0) {
			this.log.debug("Search for diary " + name + " WITHOUT results for "
					+ user.getUsername());
			return null;
		} else {
			this.log.debug("Search for diary " + name + " WITH results for "
					+ user.getUsername());
			return diaryList.get(0);
		}

	}

}
