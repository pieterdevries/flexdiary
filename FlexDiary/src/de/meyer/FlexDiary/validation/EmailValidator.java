/**
 * (c) Frank Meyer, 2014
 * All Rights Reserved
 * No part of this software or any of its contents may be reproduced, copied, 
 * modified or adapted, without the prior written consent of the author, 
 * unless otherwise indicated for stand-alone materials.
 */

package de.meyer.FlexDiary.validation;

import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;

/**
 *
 * @author Frank Meyer
 */

@FacesValidator("emailvalidation")
public class EmailValidator implements Validator {
    private ResourceBundle bundle;
    static Logger log = Logger.getLogger(EmailValidator.class);

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object value) throws ValidatorException {
        this.log.debug("Validating eMail");
        this.bundle = ResourceBundle.getBundle ("de.meyer.FlexDiary.resources.messages");
        if (!((String) value).matches(".+@.+\\..+")) {
            throw new ValidatorException(
            new FacesMessage(FacesMessage.SEVERITY_ERROR.toString(),bundle.getString("error_emailadress_not_valid")));
            
            }
        
        if (((String) value).isEmpty()) {
            throw new ValidatorException(
            new FacesMessage(FacesMessage.SEVERITY_ERROR.toString(),bundle.getString("error_no_emailadress")));
         }
       
        }

}
