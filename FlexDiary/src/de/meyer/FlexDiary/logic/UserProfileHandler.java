/**
  * (c) Frank Meyer, 2014
  * All Rights Reserved
  * No part of this software or any of its contents may be reproduced, copied, 
  * modified or adapted, without the prior written consent of the author, 
  * unless otherwise indicated for stand-alone materials.
 */

package de.meyer.FlexDiary.logic;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.apache.log4j.Logger;

import de.meyer.FlexDiary.data.MeasureUnit;
import de.meyer.FlexDiary.data.TypeOfMeasureUnits;
import de.meyer.FlexDiary.data.UserData;
import de.meyer.FlexDiary.services.UserServices;




/**
 *
 * @author Frank Meyer
 */

@ManagedBean
public class UserProfileHandler {
    
    static Logger log = Logger.getLogger(UserProfileHandler.class);
	
	@ManagedProperty(value="#{loginHandler.loggedInUser}")
	private UserData usrData;
	
	private UserServices us ;
	
	
	public UserProfileHandler() {
		this.us = new UserServices();
	}
	
    
    
    public String cancel() {
    	return "/flexdiary/welcome.xhtml?faces-redirect=true";
    }
    
    
    
    public UserData getUsrData() {
		return usrData;
	}

	public void setUsrData(UserData usrData) {
		this.usrData = usrData;
	}


	public String updateProfile() {
        
        this.log.debug("Update user profile for " + this.usrData.getUsername());
        
        this.us.updateUser(this.usrData);
        
    	return "/flexdiary/welcome.xhtml?faces-redirect=true"; 
    }



	
    
}
