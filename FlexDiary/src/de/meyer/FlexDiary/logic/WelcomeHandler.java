/**
 * 
 */
package de.meyer.FlexDiary.logic;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.apache.log4j.Logger;

import de.meyer.FlexDiary.data.UserData;

/**
 * @author thufir
 *
 */

@ManagedBean
public class WelcomeHandler implements Serializable {
    
	
    static Logger log = Logger.getLogger(LoginHandler.class);
	
	@ManagedProperty(value="#{loginHandler.loggedInUser}")
	private UserData usrData;

	
	
	public WelcomeHandler() {
		
	}
	public UserData getUsrData() {
		return usrData;
	}

	public void setUsrData(UserData usrData) {
		this.usrData = usrData;
	}
	
	
	
	
}
