/**
 * (c) Frank Meyer, 2014
 * All Rights Reserved
 * No part of this software or any of its contents may be reproduced, copied, 
 * modified or adapted, without the prior written consent of the author, 
 * unless otherwise indicated for stand-alone materials.
 */

package de.meyer.FlexDiary.logic;

import de.meyer.FlexDiary.data.Diary;
import de.meyer.FlexDiary.data.TypeOfMeasureUnits;
import de.meyer.FlexDiary.data.UserData;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.apache.log4j.Logger;

import de.meyer.FlexDiary.data.MeasureUnit;
import de.meyer.FlexDiary.services.EntryServices;
import de.meyer.FlexDiary.services.MeasureUnitServices;
import de.meyer.FlexDiary.services.UserServices;

/**
 * 
 * @author Frank Meyer
 */
@SuppressWarnings("serial")
@ManagedBean
public class InitializeTestData implements Serializable {

	private UserServices us;
	private MeasureUnitServices mus;
	private UserData testUser;
	private Diary testDiary;

	static Logger log = Logger.getLogger(InitializeTestData.class);

	public InitializeTestData() {
		this.us = new UserServices();
		this.mus = new MeasureUnitServices();
	}

	public String createTestData() {

		this.log.debug("Start creating TestData");
		this.createUserData();

		this.createMUs();

		this.connectUserMU();

		return "/index.xhtml?faces-redirect=true";
	}

	public void createUserData() {

		this.testDiary = new Diary();

		this.testDiary.setDefaultdiary(true);
		this.testDiary.setName("TestDiary");
		this.testUser = new UserData();
		this.testUser.setUsername("test");
		this.testUser.setPassword("tester");
		this.testUser.setFirstName("Daniel");
		this.testUser.setLastName("Düsentrieb");
		this.testUser.setBasalInsulin("Lantus");
		this.testUser.setBolusInsulin("Blabla");
		this.testUser.setMaxBloodGlucose(180);
		this.testUser.setMinBloodGlucose(70);
		this.testUser.setTargetBloodGlucose(100);
		this.testUser.setEmail("meyer@savvy.de");

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, 1970);
		cal.set(Calendar.MONTH, 5);
		cal.set(Calendar.DAY_OF_MONTH, 8);

		this.testUser.setDateOfBirth(cal.getTime());

		this.testDiary.setUserId(this.testUser.getId());

		this.us.saveNewDiary(this.testDiary);

		this.us.saveUser(this.testUser);

		this.log.debug("Testuser was created");

	}

	public void connectUserMU() {
		List<MeasureUnit> muBSList = this.mus.getBsmUList();
		List<MeasureUnit> muWList = this.mus.getWmUList();
		List<MeasureUnit> muCList = this.mus.getCmUList();
		List<MeasureUnit> muIList = this.mus.getImUList();
		List<MeasureUnit> muHList = this.mus.getHmUList();

		if (!muBSList.isEmpty()) {
			this.testUser.setMuBloodSugar(muBSList.get(1));
		}

		if (!muWList.isEmpty()) {
			this.testUser.setMuWeight(muWList.get(0));
		}

		if (!muCList.isEmpty()) {
			this.testUser.setMuCarbo(muCList.get(0));
		}

		if (!muIList.isEmpty()) {
			this.testUser.setMuInsulin(muIList.get(0));
		}

		if (!muHList.isEmpty()) {
			this.testUser.setMuHeight(muHList.get(0));
		}

		this.us.updateUser(this.testUser);
		this.log.debug("Testuser was updated with MU");
	}

	public void createMUs() {

		MeasureUnit mu, mu1, mu2, mu3, mu4, mu5, mu6, mu7;

		mu = new MeasureUnit();
		mu.setLabel("kg");
		mu.setType(TypeOfMeasureUnits.WEIGHT);
		mu.setDescription("Test");

		mu5 = new MeasureUnit();
		mu5.setLabel("lb");
		mu5.setType(TypeOfMeasureUnits.WEIGHT);
		mu5.setDescription("Test");

		mu1 = new MeasureUnit();

		mu1.setLabel("cm");
		mu1.setType(TypeOfMeasureUnits.HEIGHT);
		mu1.setDescription("Test");

		mu6 = new MeasureUnit();

		mu6.setLabel("inch");
		mu6.setType(TypeOfMeasureUnits.HEIGHT);
		mu6.setDescription("Test");

		mu2 = new MeasureUnit();

		mu2.setLabel("BE");
		mu2.setType(TypeOfMeasureUnits.CARBOHYDRATE);
		mu2.setDescription("Test");

		mu3 = new MeasureUnit();
		mu3.setLabel("mg/dl");
		mu3.setType(TypeOfMeasureUnits.BLOODSUGAR);
		mu3.setDescription("Test");

		mu7 = new MeasureUnit();
		mu7.setLabel("mmol");
		mu7.setType(TypeOfMeasureUnits.BLOODSUGAR);
		mu7.setDescription("Test");

		mu4 = new MeasureUnit();
		mu4.setLabel("IE");
		mu4.setType(TypeOfMeasureUnits.INSULIN);
		mu4.setDescription("Test");

		this.mus.saveNewMeasueUnit(mu);
		this.mus.saveNewMeasueUnit(mu1);
		this.mus.saveNewMeasueUnit(mu2);
		this.mus.saveNewMeasueUnit(mu3);
		this.mus.saveNewMeasueUnit(mu4);
		this.mus.saveNewMeasueUnit(mu5);
		this.mus.saveNewMeasueUnit(mu6);
		this.mus.saveNewMeasueUnit(mu7);

		this.log.debug("Measure Units where created");

	}

}