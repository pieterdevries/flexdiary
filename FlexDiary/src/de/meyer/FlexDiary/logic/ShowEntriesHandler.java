/**
 * 
 */
package de.meyer.FlexDiary.logic;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.apache.log4j.Logger;

import de.meyer.FlexDiary.data.Diary;
import de.meyer.FlexDiary.data.Entry;
import de.meyer.FlexDiary.data.UserData;
import de.meyer.FlexDiary.services.EntryServices;
import de.meyer.FlexDiary.services.UserServices;

/**
 * @author thufir
 *
 */

@ManagedBean
public class ShowEntriesHandler implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static Logger log = Logger.getLogger(ShowEntriesHandler.class);
    
	@ManagedProperty(value="#{loginHandler.loggedInUser}")
	private UserData usrData;
	
	private Diary tmpDiary;
	
	private EntryServices es;
	private UserServices us ;
	
	public ShowEntriesHandler() {
		this.es  = new EntryServices();
		this.us = new UserServices();
	}
	public List<Diary> getDiaries() {
		
		return null;
	}
	
	public String switchDairy(Diary arg) {
		return null;
	}
	
	public List<Entry> getEntries(int numberOfEntries, int upSet, Date fromDate, Date toDate) {
		
		return null;
	}

	public Diary getTmpDiary() {
		return tmpDiary;
	}

	public void setTmpDiary() {
		this.tmpDiary = tmpDiary;
	}

	public UserData getUsrData() {
		return usrData;
	}
	
	
}
