package de.meyer.FlexDiary.logic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import de.meyer.FlexDiary.data.MeasureUnit;
import de.meyer.FlexDiary.data.TypeOfMeasureUnits;
import de.meyer.FlexDiary.services.MeasureUnitServices;


@ManagedBean
public class MeasureUnitHandler implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MeasureUnitServices ms;
	private List<MeasureUnit> clocallist, hlocallist, bslocallist, wlocallist,ilocallist;
	
	
	void MausureUnitHandler() {
		this.ms = new MeasureUnitServices();
	}

	public List<MeasureUnit> getClocallist() {
		this.clocallist = this.ms.getCmUList();		
		return clocallist;
	}

	public List<MeasureUnit> getHlocallist() {
		this.hlocallist = this.ms.getHmUList();		
		return hlocallist;
	}

	public List<MeasureUnit> getBslocallist() {
		this.bslocallist = this.ms.getBsmUList();
		return bslocallist;
	}

	public List<MeasureUnit> getWlocallist() {
		this.wlocallist = this.ms.getWmUList();
		return wlocallist;
	}

	public List<MeasureUnit> getIlocallist() {
		this.ilocallist = this.ms.getImUList();
		return ilocallist;
	}

}
