/**
 * (c) Frank Meyer, 2014 All Rights Reserved No part of this software or any of
 * its contents may be reproduced, copied, modified or adapted, without the
 * prior written consent of the author, unless otherwise indicated for
 * stand-alone materials.
 */
package de.meyer.FlexDiary.logic;

import de.meyer.FlexDiary.data.UserData;
import de.meyer.FlexDiary.services.UserServices;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import org.apache.log4j.Logger;

/**
 *
 * @author thufir
 */
@ManagedBean
public class RegisterNewUserHandler implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static Logger log = Logger.getLogger(RegisterNewUserHandler.class);

    private UserData newUser = new UserData();
    private String userName;
    private String firstName;
    private String lastName;
    private String password;
    private String retypedpassword;

    private String email;
    
    private UserServices us;

    public RegisterNewUserHandler() {
        this.us = new UserServices();
    }

    public String registerNewUser() {
        

        if (this.retypedpassword.equals(this.password)) {
            this.log.debug("Password is identical with retyped password.");
            
            if (this.checkUserNamefree()) {
                //Initializing newUser
                this.log.debug("New user object will be initialized.");
                this.newUser.setLastName(this.lastName);
                this.newUser.setFirstName(this.firstName);
                this.newUser.setUsername(this.userName);
                this.newUser.setPassword(this.password);
                this.newUser.setEmail(this.email);
                this.log.debug("Initializing finished. Begin to persist new user.");
                //Persisting newUser in database


                this.us.saveUser(this.newUser);
                return "/index.xhtml?faces-redirect=true";

                } else {
                    this.log.error("New user not persisted.");
                    return "/registerNewUser.xhtml?faces-redirect=true";
                }

            } else {
                this.log.error("Username already in use.");
                return "/registerNewUser.xhtml?faces-redirect=true";
            }
        }
   

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRetypedpassword() {
        return retypedpassword;
    }

    public void setRetypedpassword(String retypedpassword) {
        this.retypedpassword = retypedpassword;
    }

    public boolean checkUserNamefree() {
        
        return this.us.checkUsernameAlreadyInUse(this.userName);
    }
}
