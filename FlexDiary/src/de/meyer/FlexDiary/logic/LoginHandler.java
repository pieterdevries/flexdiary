/**
 * (c) Frank Meyer, 2014 All Rights Reserved No part of this software or any of
 * its contents may be reproduced, copied, modified or adapted, without the
 * prior written consent of the author, unless otherwise indicated for
 * stand-alone materials.
 */
package de.meyer.FlexDiary.logic;

import de.meyer.FlexDiary.data.UserData;
import de.meyer.FlexDiary.services.UserServices;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;

import org.apache.log4j.Logger;

/**
 *
 * @author thufir
 */
@ManagedBean
public class LoginHandler implements Serializable {

    static Logger log = Logger.getLogger(LoginHandler.class); 

    private String username;
    private String password;
    private UserData loggedInUser;

    private UserServices us;
    
    
    public LoginHandler() {
    	this.us = new UserServices();


    }

    public String login() {

        this.loggedInUser = us.isLoginCorrect(this.username, this.password);

        if (this.loggedInUser != null) {
            this.log.info("Login successfull for " + this.loggedInUser.getUsername());
            if (this.us.getDefaultDiary(this.loggedInUser) != null) {
            	this.loggedInUser.setCurrDiary(this.us.getDefaultDiary(this.loggedInUser));
            	this.log.info("Default Diary assigned for user " + this.loggedInUser.getUsername() + " - " + this.loggedInUser.getCurrDiary().getName());
            }
            return "flexdiary/welcome.xhtml?faces-redirect=true";
        } else {
            this.log.warn("Login failed for " + this.username);
            return "index.xhtml?faces-redirect=true";
        }

    }

    public String logout() {

        this.log.info("Logout - " + this.loggedInUser.getUsername());

        this.loggedInUser = null;

        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        return "/login.xhtml?faces-redirect=true";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserData getLoggedInUser() {
        return loggedInUser;
    }

    public void checkedLoggedIn(ComponentSystemEvent cse) {
        FacesContext context = FacesContext.getCurrentInstance();

        if (this.loggedInUser == null) {
            this.log.warn("No access - there is no user logged in!");
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "/login.xhtml?aces-redirect=true");
        }
    }
    

}
