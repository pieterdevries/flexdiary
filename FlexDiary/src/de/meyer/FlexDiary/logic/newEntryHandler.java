/**
 * (c) Frank Meyer, 2014 All Rights Reserved No part of this software or any of
 * its contents may be reproduced, copied, modified or adapted, without the
 * prior written consent of the author, unless otherwise indicated for
 * stand-alone materials.
 */
package de.meyer.FlexDiary.logic;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.apache.log4j.Logger;

import de.meyer.FlexDiary.data.Entry;
import de.meyer.FlexDiary.data.UserData;
import de.meyer.FlexDiary.services.EntryServices;
import de.meyer.FlexDiary.services.UserServices;

/**
 * @desc
 * @author thufir
 * 
 */

@ManagedBean
public class newEntryHandler implements Serializable {

	static Logger log = Logger.getLogger(newEntryHandler.class);

	@ManagedProperty(value = "#{loginHandler.loggedInUser}")
	private UserData usrData;

	private UserServices us; 
	private EntryServices es;

	private Entry tmpEntry;
	
	public newEntryHandler() {
		this.us = new UserServices();
		this.es  = new EntryServices();
		
	}

	public String saveEntry() {
		this.log.debug("Save entry for user" + usrData.getUsername() + " for "
				+ usrData.getCurrDiary() + "-Diary.");
		this.tmpEntry.setDiaryID(usrData.getCurrDiary().getId());
		this.tmpEntry.setUserID(usrData.getId());
		this.es.saveNewEntry(this.tmpEntry);
		return "showEntries.xhtml?faces-redirect=true";
	}

}
