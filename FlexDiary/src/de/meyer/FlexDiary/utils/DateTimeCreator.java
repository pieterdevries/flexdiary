/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.meyer.FlexDiary.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Klasse zum Formatieren von Datumsausdruecken. Wird benutzt bei der Erzeugung
 * von SQL-Statements.
 * 
 * @author Frank Meyer
 * @version 1.0
 * @updated 08-Jan-2012 20:20:19
 */

public class DateTimeCreator {

    private String tmpString;
    private Date tmpDate;
    private Timestamp tmpTimestamp;
    private Calendar tmpCalendar;
    private SimpleDateFormat FormatterDate = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat FormatterTime = new SimpleDateFormat("HHmmss");
    private SimpleDateFormat FormatterGUIDate = new SimpleDateFormat(
	    "dd.MM.yyyy");
    private SimpleDateFormat FormatterTimeStamp = new SimpleDateFormat(
	    "yyyyMMddHHmmss");
    private SimpleDateFormat FormatterTimeStampGUI = new SimpleDateFormat(
	    "dd.MM.yyyy HH:mm");
    private SimpleDateFormat FormatterTimeStampReading = new SimpleDateFormat(
	    "dd.MM.yyyy HH:mm:ss");

    public DateTimeCreator() {

    }

    /**
     * Diese Funktion wandelt ein Datum des Typs java.util.Date in einen String
     * um.
     * 
     * @param Date
     *            arg
     * @author Frank Meyer
     * 
     */
    public String getDate(Date arg) {
	try {
	    tmpString = FormatterDate.format(arg);
	    return tmpString;
	} catch (Exception e) {
	    return null;
	}

    }

    /**
     * Diese Funktion wandelt ein Datum des Typs java.util.Date in einen String
     * um. Hierbei wird ein String erzeugt f�r die Anzeige auf der Oberfl�che.
     * 
     * Typische Verwendung in der Anzeige von Von/Bis-Datum in der
     * Tabellenansicht f�r die Tagebucheintr�ge.
     * 
     * @param Date
     *            arg
     * @author Frank Meyer
     * 
     */
    public String getDateGUI(Date arg) {
	try {
	    tmpString = FormatterGUIDate.format(arg);
	    return tmpString;
	} catch (Exception e) {
	    return null;
	}

    }

    /**
     * Diese Funktion stellt die Umwandlung von Typ java.uti.Date in eine
     * Zeitanzeige um.
     * 
     * @param Date
     *            arg
     * @author Frank Meyer
     * @return tmpString
     */
    public String getTime(Date arg) {
	try {
	    tmpString = FormatterTime.format(arg);
	    return tmpString;
	} catch (Exception e) {
	    return null;
	}

    }

    /**
     * Diese Funktion erstellt den TimeStamp f�r die Datumspeicherung in der
     * Datenbank.
     * 
     * @param arg
     * @author Frank Meyer
     * @return tmpString
     */
    public String getTimeStamp(Date arg) {
	try {
	    tmpString = FormatterTimeStamp.format(arg);

	    return tmpString;
	} catch (Exception e) {

	    return null;
	}

    }

    /**
     * Funktion zum Einlesen eines Strings und Umwandlung in ein java.util.Date
     * 
     * @param arg
     * @author Frank Meyer
     * @return tmpDate
     */
    public Date getTimeStamp(String arg) {
	try {
	    tmpDate = FormatterTimeStamp.parse(arg);

	    return tmpDate;
	} catch (Exception e) {

	    return null;
	}

    }

    /**
     * Funktion zum Umwandeln eines Datums in eine Datumsanzeige f�r das GUI.
     * 
     * @param arg
     * @author Frank Meyer
     * @return tmpString
     */
    public String getTimeStampGUI(Date arg) {
	try {
	    tmpString = FormatterTimeStampGUI.format(arg);

	    return tmpString;
	} catch (Exception e) {

	    return null;
	}
    }

    /**
     * Methode zum Einlesen des Timestamp aus der Datenbank und Umwandlung in
     * ein java.util.Date.
     * 
     * @param arg
     * @author Frank Meyer
     * @return tmpDate
     */
    public Date getTimeStampRead(String arg) {
	try {
	    tmpDate = FormatterTimeStampReading.parse(arg);

	    return tmpDate;
	} catch (Exception e) {

	    return null;
	}

    }

    public Timestamp formatTimestamp(Timestamp arg) {
	try {
	    tmpCalendar.setTimeInMillis(arg.getTime());
	    tmpTimestamp = (Timestamp) FormatterTimeStamp.parse(tmpCalendar
		    .getTime().toString());

	    return tmpTimestamp;

	} catch (Exception e) {

	    return null;
	}
    }
    
    public Date getTCLessDate(Date date) {
        try{
            this.tmpCalendar.setTime(date);
            this.tmpCalendar.setTimeZone(TimeZone.getTimeZone("CET"));
            return this.tmpCalendar.getTime();
        }catch (Exception e) {
            return null;
        }
    }

}

