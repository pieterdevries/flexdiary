/**
 * (c) Frank Meyer, 2014
 * All Rights Reserved
 * No part of this software or any of its contents may be reproduced, copied, 
 * modified or adapted, without the prior written consent of the author, 
 * unless otherwise indicated for stand-alone materials.
 */

package de.meyer.FlexDiary.data;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 * Contains a entry in the diary.
 * 
 * @author Frank Meyer
 * @version 1.0
 * @updated 20-Mrz-2012 19:16:18
 */
@Entity
public class Entry {

    @Id
    @GeneratedValue
    private String id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createdtimestamp;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date entrydate;
    private String descactivity;
    private String descmeal;
    private boolean beforeactivity;
    private boolean beforemeal;
    private String remark;
    private int valuebe;
    private String diaryID;
    private String activityID;
    private String userID;
    private int valuebloodglucose;
    private int weight;
    private int bolusinsulin;
    private int basalinsulin;

    public Entry() {
        this.id = UUID.randomUUID().toString();

    }

    /**
     * @return the createdtimestamp
     */
    public Date getCreatedtimestamp() {
	return createdtimestamp;
    }

    /**
     * @param createdtimestamp
     *            the createdtimestamp to set
     */
    public void setCreatedtimestamp(Date createdtimestamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getDefault());
        
        cal.setTime(createdtimestamp);
	
        this.createdtimestamp = cal.getTime();
    }

    /**
     * @return the entrydate
     */
    public Date getEntrydate() {
       
	return this.entrydate;
    }

    /**
     * @param entrydate
     *            the entrydate to set
     */
    public void setEntrydate(Date entrydate) {
      
        this.entrydate = entrydate;
    }

    /**
     * @return the descactivity
     */
    public String getDescactivity() {
	return descactivity;
    }

    /**
     * @param descactivity
     *            the descactivity to set
     */
    public void setDescactivity(String descactivity) {
	this.descactivity = descactivity;
    }

    /**
     * @return the descmeal
     */
    public String getDescmeal() {
	return descmeal;
    }

    /**
     * @param descmeal
     *            the descmeal to set
     */
    public void setDescmeal(String descmeal) {
	this.descmeal = descmeal;
    }

    /**
     * @return the beforeactivity
     */
    public boolean getBeforeactivity() {
	return beforeactivity;
    }

    /**
     * @param isBeforeActivity
     *            the beforeactivity to set
     */
    public void setBeforeactivity(boolean isBeforeActivity) {
	this.beforeactivity = isBeforeActivity;
    }

    /**
     * @return the beforemeal
     */
    public boolean getBeforemeal() {
	return beforemeal;
    }

    /**
     * @param isBeforeMeal
     *            the beforemeal to set
     */
    public void setBeforemeal(boolean isBeforeMeal) {
	this.beforemeal = isBeforeMeal;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
	return remark;
    }

    /**
     * @param remark
     *            the remark to set
     */
    public void setRemark(String remark) {
	this.remark = remark;
    }

    /**
     * @return the valuebe
     */
    public int getValuebe() {
	return valuebe;
    }

    /**
     * @param valueBE
     *            the valuebe to set
     */
    public void setValuebe(int valueBE) {
	this.valuebe = valueBE;
    }

    /**
     * @return the diaryID
     */
    public String getDiaryID() {
	return diaryID;
    }

    /**
     * @param diaryID
     *            the diaryID to set
     */
    public void setDiaryID(String diaryID) {
	this.diaryID = diaryID;
    }

    /**
     * @return the typeOfAction
     */
    public String getActivityId() {
	return activityID;
    }

    /**
     * @param typeOfAction
     *            the typeOfAction to set
     */
    public void setActivity(String typeOfAction) {
	this.activityID = typeOfAction;
    }

    /**
     * @return the userID
     */
    public String getUserID() {
	return userID;
    }

    /**
     * @param userID
     *            the userID to set
     */
    public void setUserID(String userID) {
	this.userID = userID;
    }

    /**
     * @return the valuebloodglucosemgdl
     */

    public int getBolusinsulin() {
	return bolusinsulin;
    }

    public void setBolusinsulin(int bolusinsulin) {
	this.bolusinsulin = bolusinsulin;
    }

    public int getBasalinsulin() {
	return basalinsulin;
    }

    public void setBasalinsulin(int basalinsulin) {
	this.basalinsulin = basalinsulin;
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public int getValuebloodglucose() {
        return valuebloodglucose;
    }

    public void setValuebloodglucose(int valuebloodglucose) {
        this.valuebloodglucose = valuebloodglucose;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    
    
}