package de.meyer.FlexDiary.data;

import java.util.UUID;

import javax.faces.model.SelectItem;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class MeasureUnit extends SelectItem{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String label;
	private String value;
	private String type;
	private String description;
	
	
	
	
	public MeasureUnit() {
        this.id = UUID.randomUUID().toString();
	}




	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public String getLabel() {
		return label;
	}




	public void setLabel(String label) {
		this.label = label;
	}




	public String getDescription() {
		return description;
	}




	public void setDescription(String description) {
		this.description = description;
	}




	public String getType() {
		return type;
	}




	public void setType(TypeOfMeasureUnits type) {
		this.type = type.toString();
	}




	public String getValue() {
		return value;
	}




	public void setValue(String value) {
		this.value = value;
	}
	
	

}
