package de.meyer.FlexDiary.data;

public enum TypeOfMeasureUnits {
	WEIGHT, HEIGHT, BLOODSUGAR, CARBOHYDRATE, INSULIN;

}
