/**
 * (c) Frank Meyer, 2014
 * All Rights Reserved
 * No part of this software or any of its contents may be reproduced, copied, 
 * modified or adapted, without the prior written consent of the author, 
 * unless otherwise indicated for stand-alone materials.
 */

package de.meyer.FlexDiary.data;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author thufir
 */
@Entity
public class Diary  implements Serializable {

    private String name;
    private boolean defaultdiary;
    private String userid;
    
    @Id
    @GeneratedValue
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDefaultdiary() {
        return defaultdiary;
    }

    public void setDefaultdiary(boolean defaultdiary) {
        this.defaultdiary = defaultdiary;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
		return userid;
	}

	public void setUserId(String userId) {
		this.userid = userId;
	}

	public void setId(String id) {
        this.id = id;
    }
    
    
    public Diary() {
        this.id = UUID.randomUUID().toString();
    }

    
}
