/**
 * (c) Frank Meyer, 2014
 * All Rights Reserved
 * No part of this software or any of its contents may be reproduced, copied, 
 * modified or adapted, without the prior written consent of the author, 
 * unless otherwise indicated for stand-alone materials.
 */

package de.meyer.FlexDiary.data;


import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.Transient;



/**
 * @author Frank Meyer
 * @version 1.0
 * @created 08-Jan-2012 22:22:32
 */
@Entity
public class UserData implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    private String id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateOfBirth;
    private int status;
    private String basalInsulin;
    private String bolusInsulin;
    private int height;
    private MeasureUnit muHeight;
    private MeasureUnit muInsulin;
    private MeasureUnit muWeight;
    private MeasureUnit muCarbo;
    private MeasureUnit muBloodSugar;
    private int maxBloodGlucose;
    private int minBloodGlucose;
    private int targetBloodGlucose;
    private int weight;
    private String firstName;
    private String username;
    private String password;
    private String lastName;
    private String email;
    
    /* @desc This attribute will be only used, when the application is running.
     * Saving the value is not necessary.
     * 
     */    
    @Transient
    private Diary currDiary;



    public UserData() {
        this.id = UUID.randomUUID().toString();

    }

    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }    

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getBasalInsulin() {
        return basalInsulin;
    }

    public void setBasalInsulin(String basalInsulin) {
        this.basalInsulin = basalInsulin;
    }

    public String getBolusInsulin() {
        return bolusInsulin;
    }

    public void setBolusInsulin(String bolusInsulin) {
        this.bolusInsulin = bolusInsulin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getMaxBloodGlucose() {
        return maxBloodGlucose;
    }

    public void setMaxBloodGlucose(int maxBloodGlucose) {
        this.maxBloodGlucose = maxBloodGlucose;
    }

    public int getMinBloodGlucose() {
        return minBloodGlucose;
    }

    public void setMinBloodGlucose(int minBloodGlucose) {
        this.minBloodGlucose = minBloodGlucose;
    }

    public int getTargetBloodGlucose() {
        return targetBloodGlucose;
    }

    public void setTargetBloodGlucose(int targetBloodGlucose) {
        this.targetBloodGlucose = targetBloodGlucose;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Diary getCurrDiary() {
        return currDiary;
    }

    public void setCurrDiary(Diary currDiary) {
        this.currDiary = currDiary;
    }


	public MeasureUnit getMuHeight() {
		return muHeight;
	}


	public void setMuHeight(MeasureUnit muHeight) {
		this.muHeight = muHeight;
	}


	public MeasureUnit getMuInsulin() {
		return muInsulin;
	}


	public void setMuInsulin(MeasureUnit muInsulin) {
		this.muInsulin = muInsulin;
	}


	public MeasureUnit getMuWeight() {
		return muWeight;
	}


	public void setMuWeight(MeasureUnit muWeight) {
		this.muWeight = muWeight;
	}


	public MeasureUnit getMuCarbo() {
		return muCarbo;
	}


	public void setMuCarbo(MeasureUnit muCarbo) {
		this.muCarbo = muCarbo;
	}


	public MeasureUnit getMuBloodSugar() {
		return muBloodSugar;
	}


	public void setMuBloodSugar(MeasureUnit muBloodSugar) {
		this.muBloodSugar = muBloodSugar;
	}
    
}


